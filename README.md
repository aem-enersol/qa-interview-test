[![AEM Enersol](http://i0.wp.com/aemenersol.com/wp-content/uploads/2015/12/Logo-AEM-for-MegaProject-Final.png?fit=290%2C129)](http://aemenersol.com)

AEM Enersol is an independent integrated consultancy services, from upstream to downstream. Our impartiality allows us to provide a high quality advise to optimize clients' portfolio in a business. Our principle is grounded in an ultimate priority - achieving clients' needs at beyond the best limit.

# QA Interview Test Overview

The role of QA in agile can involve both testing and development. The idea is that developers and testers must actively engage to deliver the code and complete the project as per the client's brief. QA helps to proactively address issues and potential bugs within an application during the development cycles

QA helps to proactively address issues and potential bugs within an application during the development cycles. It can also help address functionality, performance, or security issues. This will not only ensure stability of the application, but also bring down the testing efforts once the application lands in the hands of the users. In this way developers are able to move ahead without dealing relentlessly with pending issues or rework.

In AEM, we recently introduce this role with the objective to improve our software development process and quality as well. We encourage the candidate to understand the basic flow of software testing and at same time we look into the certified testers as well (not mandatory) e.g. ISTQB.

## Guideline

You need to clone this repository and you may use whatever resources you like as long as you are following the below **Do and Don't**.

## Do and Don't
   - Do clone this repository
   - Don't use any existing angular component, example [ng-select](https://ng-select.github.io/ng-select)
   - Do create your own tagging input implementation and place it under shared folder.
   - Do host your finished code in Git provider (Gitlab, Github or Bitbucket) and you will required to send us the link.

## Your task

Prepare a slide presentation explaining your understanding of the QA role in software development and how QA contributes to improving software quality. Your presentation should cover the following aspects:

1.	QA Role & Importance:
   - What is the role of QA in the software development lifecycle (SDLC)?
   - How does QA contribute to improving software quality and overall user experience? Does QA an essential part of an organization?


2.	QA Testing Processes & Methodologies:
   - Understanding the QA Testing Life Cycle.
   - Best practices for writing test cases and test plans, difference between manual testing and automated testing—when to use each.
   - Provide types of testing.


3.	Automation Testing:
   - What is automation testing, and why is it important?
   - How to design and maintain automated test scripts for regression testing and enhancement testing?
   - Describe role of automation in CI/CD pipelines.


4.	Performance Testing:
   - What is performance testing, and is it essential for a system/application?
   - List types of performance testing – purpose and use case.
  
       *Example use case: Stress Testing – Access real-time offshore drilling monitoring dashboard simultaneously, causing sudden high traffic.*
   - What are the key metrics in performance test? How to analyse performance test results and improve system performance?


5.	QA’s Role in Agile & DevOps:
   - How QA integrates into Agile and DevOps workflows?
   - Describe Shift-Left Testing approach and its benefits.

